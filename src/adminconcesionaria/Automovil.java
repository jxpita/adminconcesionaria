/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

/**
 * Una clase para representar un Vehiculo de tipo Automovil
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Automovil extends Vehiculo{
    private int numAsientos;
    private boolean esConvertible;
    private boolean tieneCamaraRetro;

    public Automovil() {
    }

    public Automovil(int numAsientos, boolean esConvertible, boolean tieneCamaraRetro, String marca, String modelo, String anioDeFabricacion, String tipoGasolina, int kilometraje) {
        super(marca, modelo, anioDeFabricacion, tipoGasolina, 4, kilometraje);// Solo tiene 4 llantas
        this.numAsientos = numAsientos;
        this.esConvertible = esConvertible;
        this.tieneCamaraRetro = tieneCamaraRetro;
    }

    /**
     * Metodo para obtener atributo privado que representa el numero de asientos del Automovil
     * @return atributo que representa el numero de asientos del Automovil
     */
    public int getNumAsientos() {
        return numAsientos;
    }

    /**
     * Metodo para modificar valor del atributo numAsientos
     * @param numAsientos representa el numero de asientos del Automovil
     */
    public void setNumAsientos(int numAsientos) {
        this.numAsientos = numAsientos;
    }

    /**
     * Metodo que devuelve el valor booleano del atributo esConvertible
     * @return valor booleano que determina si el Automovil es convertible
     */
    public boolean isEsConvertible() {
        return esConvertible;
    }

    /**
     * Metodo para modificar valor del atributo esConvertible
     * @param esConvertible representa valor booleano que determina si el Automovil es convertible
     */
    public void setEsConvertible(boolean esConvertible) {
        this.esConvertible = esConvertible;
    }

    /**
     * Metodo que devuelve valor booleano del atributo tieneCamaraRetro
     * @return valor booleano que determina si el Automovil tiene camara retro
     */
    public boolean isTieneCamaraRetro() {
        return tieneCamaraRetro;
    }

    /**
     * Metodo para modificar valor del atributo tieneCamaraRetro
     * @param tieneCamaraRetro representa valor booleano que determina si el Automovil tiene camara retro
     */
    public void setTieneCamaraRetro(boolean tieneCamaraRetro) {
        this.tieneCamaraRetro = tieneCamaraRetro;
    }
}
