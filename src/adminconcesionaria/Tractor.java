/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

/**
 * Una clase que representa un Vehiculo de tipo Tractor
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Tractor extends Vehiculo{
    private boolean esAgricola;
    private String transmision; // Transmisión hidraulica o mecanica

    public Tractor() {
        super();
    }

    public Tractor(boolean esAgricola, String transmision, String marca, String modelo, String anioDeFabricacion, int kilometraje) {
        super(marca, modelo, anioDeFabricacion, "diesel", 4, kilometraje);// Solo trabaja con diesel. // Solo Tiene 4 llantas.
        this.esAgricola = esAgricola;
        this.transmision = transmision;
    }

    /**
     * Metodo que verifica si el Tractor es agricola
     * @return valor booleano del atributo esAgricola
     */
    public boolean isEsAgricola() {
        return esAgricola;
    }

    /**
     * Metodo para modificar si el Tractor es agricola
     * @param esAgricola representa booleano si el Tractor es agricola
     */
    public void setEsAgricola(boolean esAgricola) {
        this.esAgricola = esAgricola;
    }

    /**
     * Metodo para obtener el tipo de transmision de un Tractor
     * @return String del tipo de transmision de un Tractor
     */
    public String getTransmision() {
        return transmision;
    }

    /**
     * Metodo para modificar el tipo de transmision de un Tractor
     * @param transmision
     */
    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }
}
