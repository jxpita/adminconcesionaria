/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Una clase para representar a una Persona de tipo Supervisor
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Supervisor extends Persona implements  Solicitable{
    private ArrayList<String> listaCertificacionesAcademicas;

    public Supervisor() {
        super();
    }

    public Supervisor(String username, String password, String nombres, String apellidos, ArrayList<String> listaCertificacionesAcademicas) {
        super(username, password, nombres, apellidos);
        this.listaCertificacionesAcademicas = listaCertificacionesAcademicas;
    }

    @Override
    public  void aceptarSolicitud(String idVehiculo)throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudes.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");
            if (datos[3].equals(idVehiculo) ) {
                ManejoArchivos.escribirEnArchivo("solicitudesFinalizadas.txt", datos[0] + "," + idVehiculo + "," + "aceptado" + "," + datos[1] + "," + "Su solicitud de ccmpra ha sido aceptada. Puede acercarse a retirar su vehiculo en la concesionaria cuando desee.");
                lineas.remove(linea);
                ManejoArchivos.sobreescribirArchivo("solicitudes.txt", lineas); 
                break;
            }      
            if(lineas.isEmpty()){
                break;
            }
       }
     }

    @Override
    public void rechazarSolicitud(String idVehiculo) throws Exception{
        Scanner sc = new Scanner(System.in);
        System.out.println("\nIngrese el motivo por el cual va a rechazar la solicitud: ");
        String mensaje = sc.next();
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudes.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");
                if (datos[3].equals(idVehiculo) ){
                    ManejoArchivos.escribirEnArchivo("solicitudesFinalizadas.txt",datos[0] + "," + idVehiculo + "," + "rechazado" + "," + datos[1] + "," + mensaje);
                    lineas.remove(linea);
                    ManejoArchivos.sobreescribirArchivo("solicitudes.txt", lineas);
                    break;
                }
            if(lineas.isEmpty()){
                break;
            }
        }
    }

    /**
     * Metodo para obtener todas las certificaciones academicas de un Supervisor
     * @return ArrayList de tipo String que contiene todas las certificaciones 
     * academicas de un Supervisor
     */
    public ArrayList<String> getListaCertificacionesAcademicas() {
        return listaCertificacionesAcademicas;
    }

    /**
     * Metodo para modificar las certificaciones academicas de un Supervisor
     * @param listaCertificacionesAcademicas ArrayList de tipo String que contiene todas
     * las certificaciones academicas de un Supervisor
     */
    public void setListaCertificacionesAcademicas(ArrayList<String> listaCertificacionesAcademicas) {
        this.listaCertificacionesAcademicas = listaCertificacionesAcademicas;
    }

    @Override
    public String toString() {
        return super.toString() + "Supervisor{" + "listaCertificacionesAcademicas=" + listaCertificacionesAcademicas + '}';
    }

   
}