package adminconcesionaria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Interfaz Consultable se implementa en Cliente y Vendedor para que puedan consultar los vehiculos 
 * disponibles en stock de concesionaria
 * @author Juan Xavier Pita
 * @author 
 */
public interface Consultable {
    
    /**
     * Metodo para consultar el stock de concesionaria
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    void consultarStock() throws Exception;
    
}
