/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Una clase para representar menu principal y de clientes, vendedores, supervisores y jefe de taller
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Menu {
    
    Scanner scan = new Scanner(System.in);
    
    /**
     * Metodo para mostrar el menu principal del programa
     */
    public void menuPrincipal() {
        System.out.println("\n\nBIENVENIDO A LA CONCESIONARIA\n");
        System.out.println("1. Iniciar sesion\n"
                         + "2. Crear usuario\n"
                         + "3. Salir\n");
        System.out.print("\nElija una opción: ");
    }
    
    /**
     * Metodo para hacer login - inicio de sesion
     * @param opMenu String que contiene la opcion ingresada por teclado
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void loginMenuPrincipal(String opMenu) throws Exception {
        switch(opMenu){
            case "1":
                String username;
                String password;
                System.out.println("\nIngrese los datos para ingresar al sistema.");
                System.out.print("Usuario:\t");
                username = scan.next().trim();
                System.out.print("Contraseña:\t");
                password = scan.next().trim();
                ArrayList<String> verificacion = verificarLogin(username, password);
                if (Boolean.parseBoolean(verificacion.get(2))) {
                    String opSubmenu = "";
                    Persona usuario;
                    switch(verificacion.get(1)) {
                        case "cliente":
                            usuario = this.obtenerInfoUser(verificacion, "clientes.txt");
                            while (!opSubmenu.equals("9")) {
                                System.out.print("\n\n\nCLIENTE:\t" + usuario.getNombres());
                                menuCliente();
                                opSubmenu = scan.next().trim();
                                ingresarMenuCliente(opSubmenu, (Cliente) usuario);
                            }
                            break;
                        
                        case "vendedor":
                            usuario = this.obtenerInfoUser(verificacion, "vendedores.txt");
                            while (!opSubmenu.equals("3")) {
                                System.out.print("\n\n\nVENDEDOR:\t" + usuario.getNombres());
                                menuVendedor();
                                opSubmenu = scan.next().trim();
                                ingresarMenuVendedor(opSubmenu, (Vendedor) usuario);                            
                            }
                            break;
                                
                        case "supervisor":
                            usuario = this.obtenerInfoUser(verificacion, "certificaciones.txt");
                            while (!opSubmenu.equals("2")) {
                                System.out.print("\n\n\nSUPERVISOR:\t" + usuario.getNombres());
                                menuSupervisor();
                                opSubmenu = scan.next().trim();
                                ingresarMenuSupervisor(opSubmenu, (Supervisor)usuario);                            
                            }
                            break;
                            
                        case "jefe de taller":
                            usuario = this.obtenerInfoUser(verificacion, "certificaciones.txt");
                            while (!opSubmenu.equals("4")) {
                                System.out.print("\n\n\nJEFE DE TALLER:\t" + usuario.getNombres());
                                menuJefeTaller();
                                opSubmenu = scan.next().trim();
                                ingresarMenuJefeTaller(opSubmenu, (JefeTaller)usuario);                            
                            }
                            break;
                    }
                } else {
                    System.out.println("No se ha podido iniciar sesión");
                }
                break;

            case "2":

                break;    

            case "3":
                System.exit(0);
                break;

            default:
                System.out.println("\nHa ingresado una opción incorrecta.\nIntente nuevamente\n");
                opMenu = "3";
                break;
        }
    }
    
    /**
     * Metodo para mostrar menu de clientes
     */
    public void menuCliente() {
        System.out.println("\nMENU DE CLIENTES\n");
        System.out.println("1. Consultar stock de concesionaria\n"
                         + "2. Solicitar cotización de vehículo\n"
                         + "3. Consultar solicitud(es) de cotización\n"
                         + "4. Solicitar compra de vehículo\n" 
                         + "5. Consultar solicitud(es) de compra\n"
                         + "6. Solicitar mantenimiento de vehículo\n" //
                         + "7. Consultar solicitud(es) de mantenimiento\n"
                         + "8. Autos en mantenimiento\n"
                         + "9. Pagar Auto\n"
                         + "10. Cerrar sesión\n");
        System.out.print("\nElija una opción: ");
    }
    
    /**
     * Metodo para que el Cliente pueda consultar stock o realizar algun tipo de solicitud
     * @param opcion String que contiene la opcion ingresada por el Cliente
     * @param cliente representa al Cliente que hizo login
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void ingresarMenuCliente(String opcion, Cliente cliente) throws Exception {
        switch(opcion){
            case "1":
                cliente.consultarStock();
                break;
                
            case "2":
                System.out.print("\nIngrese un código de vehículo:\n- ");
                cliente.solicitarCotizacion(scan.next().trim());
                break;
                
            case "3":
                this.mostrarSolicitudesPendientes("cotizacion", cliente.getCedula());
                this.mostrarSolicitudesAceptadas("cotizacion", cliente.getCedula());
                break;
                
            case "4":
                System.out.print("\nIngrese un código de vehículo:\n- ");
                cliente.solicitarCompra(scan.next().trim());
                break;
                
            case "5":
                this.mostrarSolicitudesPendientes("compra", cliente.getCedula());
                this.mostrarSolicitudesAceptadas("compra", cliente.getCedula());
                break;
                
            case "6":
                System.out.print("\nIngrese un código de vehículo:\n- ");
                cliente.solicitarMantenimiento(scan.next().trim());                
                break;  
                
            case "7":
                System.out.println("\nEsta es la consulta de solicitud(es) de mantenimiento\n");
                this.mostrarSolicitudesPendientes("mantenimiento", cliente.getCedula());
                this.mostrarSolicitudesAceptadas("mantenimiento", cliente.getCedula());
                
                break;    
                
            case "8":
                this.mostrarEstadoMantenimientoAutos(cliente.getCedula());
                break;    
                
            case "9":
                System.out.print("\nIngrese un código de vehículo que desea pagar:\n- ");
                cliente.pagarDeudaMantenimiento(scan.next().trim(),cliente.getIngresos());
                break;
                
            case "10":
                System.out.println("\nSesión cerrada satisfactoriamente\n");
                break;
                
            default:
                System.out.println("\nHa ingresado una opción incorrecta\n");
                break;
        }
    }
    
    /**
     * Metodo para mostrar el menu de vendedores
     */
    public void menuVendedor() {
        System.out.println("\n\nMENU DE VENDEDOR\n");
        System.out.println("1. Solicitudes del cliente\n"
                         + "2. Consultar stock de concesionaria\n"
                         + "3. Cerrar sesión\n");
        System.out.print("\nElija una opción: ");
    }
    
    /**
     * Metodo para que el Vendedor pueda consultar stock y aceptar/rechazar solicitudes de cotizacion de un Cliente
     * @param opcion String que contiene la opcion ingresada por el Vendedor
     * @param vendedor representa al Vendedor que hizo login
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void ingresarMenuVendedor(String opcion, Vendedor vendedor) throws Exception {
        switch(opcion){
            case "1":
                System.out.println("Estas son las solicitudes del cliente");
                this.mostrarSolicitudes("cotizacion");
                System.out.println("Ingrese la solicitud que quiere acceder usando el codigo del vehiculo: ");
                String idVehiculoSolicitud = scan.next();
                System.out.println("Que desea hacer con esta solicitud: ");
                System.out.println("1. Aceptar solicitud\n"
                                 + "2. Rechazar solicitud \n");
                System.out.println("Elija una opcion: ");
                String opcionSolicitud = scan.next();
                switch(opcionSolicitud){
                    case "1":
                        vendedor.aceptarSolicitud(idVehiculoSolicitud);
                        break;
                    case "2":
                        vendedor.rechazarSolicitud(idVehiculoSolicitud);
                        break;
                    default:
                        break;
            
                }
                break;
                
            case "2":
                System.out.println("Esta es una consulta de stock de la concesionaria");
                vendedor.consultarStock();
                break;
                
            case "3":
                System.out.println("\nSesión cerrada satisfactoriamente\n");
                break;
                
            default:
                System.out.println("\nHa ingresado una opción incorrecta\n");
                break;
        }
    }
    
    /**
     * Metodo para mostrar el menu de supervisores
     */
    public void menuSupervisor() {
        System.out.println("\n\nMENU DE SUPERVISOR\n");
        System.out.println("1. Solicitudes de compra\n"
                         + "2. Cerrar sesión\n");
        System.out.print("\nElija una opción: ");
    }
    
    /**
     * Metodo para que el Supervisor aceptar/rechazar solicitudes de compra de un Cliente
     * @param opcion String que contiene la opcion ingresada por el Supervisor
     * @param supervisor representa al Supervisor que hizo login
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void ingresarMenuSupervisor(String opcion, Supervisor supervisor) throws Exception{
        switch(opcion){
            case "1":
//                System.out.println("Estas son las solicitudes de compra");
                this.mostrarSolicitudes("compra");
                System.out.println("Ingrese la solicitud que quiere acceder usando el codigo del vehiculo: ");
                String idVehiculoSolicitud = scan.next();
                System.out.println("Que desea hacer con esta solicitud: ");
                System.out.println("1. Aceptar solicitud\n"
                                 + "2. Rechazar solicitud \n");
                System.out.println("Elija una opcion: ");
                String opcionSolicitud = scan.next();
                switch(opcionSolicitud){
                    case "1":
                        supervisor.aceptarSolicitud(idVehiculoSolicitud);
                        break;
                    case "2":
                        supervisor.rechazarSolicitud(idVehiculoSolicitud);
                        break;
                    default:
                        break;
            
                }
                break;
                
            case "2":
                System.out.println("\nSesión cerrada satisfactoriamente\n");
                break;
                
            default:
                System.out.println("\nHa ingresado una opción incorrecta\n");
                break;
        }
    }
    
    /**
     * Metodo para mostrar el menu del Jefe de Taller
     */
    public void menuJefeTaller() {
        System.out.println("\n\nMENU DE JEFE DE TALLER\n");
        System.out.println("1. Entregar vehículos nuevos\n"
                         + "2. Solicitudes de mantenimiento\n"
                         + "3. Autos en mantenimiento.\n"
                         + "4. Cerrar sesión\n");
        System.out.print("\nElija una opción: ");
    }
    
    /**
     * Metodo para que el Jefe de Taller entregue vehiculos nuevos; acepte o rechace solicitudes de 
     * mantenimiento realizadas por un Cliente y realizar mantenimientos de vehiculos
     * @param opcion String que contiene la opcion ingresada por el Jefe de Taller
     * @param jefeTaller representa al Jefe de Taller que hizo login
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void ingresarMenuJefeTaller(String opcion, JefeTaller jefeTaller) throws Exception {
        switch(opcion){
            case "1":
                this.mostrarVehiculosPorEntregar();
                System.out.println(" Ingrese el codigo del vehiculo que quiere entregar: ");
                String idVehiculoEntregar = scan.next();
                jefeTaller.entregarVehiculo(idVehiculoEntregar);
                break;
                
            case "2":
                this.mostrarSolicitudes("mantenimiento");
                System.out.println("Ingrese la solicitud que quiere acceder usando el codigo del vehiculo: ");
                String idVehiculoSolicitud = scan.next();
                System.out.println("\nQue desea hacer con esta solicitud:\n");
                System.out.println("1. Aceptar solicitud\n"
                                 + "2. Rechazar solicitud \n");
                System.out.println("Elija una opcion: ");
                String opcionSolicitud = scan.next();
                switch(opcionSolicitud){
                    case "1":
                        String datoSolicitud[] = new String[4];
                        datoSolicitud = jefeTaller.obtenerSolicitud(idVehiculoSolicitud);
                        jefeTaller.aceptarSolicitud(idVehiculoSolicitud);
                        jefeTaller.ingresarVehiculoAMantenimiento(idVehiculoSolicitud, datoSolicitud[1], datoSolicitud[2], jefeTaller.obtenerKilometrajeActual(idVehiculoSolicitud));
                        break;
                    case "2":
                        jefeTaller.rechazarSolicitud(idVehiculoSolicitud);
                        break;
                    default:
                        break;
            
                }
                break;
            case "3":
                this.mostrarVehiculosEnMantenimiento();
                System.out.println("\nQue accion desea realizar: ");
                System.out.println("1. Cambiar estado de un vehiculo\n"
                                 + "2. Sacar vehiculo de mantenimiento \n");
                System.out.println("Elija una opcion: ");
                String opcionAccion = scan.next();
                String idVehiculo;
                switch(opcionAccion){
                    case "1":
                        System.out.println("Ingrese el codigo del vehiculo que quiere cambiar de estado: ");
                        idVehiculo = scan.next();  
                        System.out.println("Ingrese a que estado quiere cambiar el vehiculo: ");
                        String estadoVehiculo = scan.next();
                        jefeTaller.cambiarEstadoVehiculo(idVehiculo, estadoVehiculo);
                        System.out.println("\nEl vehiculo cambio a estado " + estadoVehiculo + "satisfactoriamente.\n" );
                        break;
                    case "2":
                        System.out.println("Ingrese el codigo del vehiculo que quiere sacar de mantenimiento: ");
                        idVehiculo = scan.next();
                        jefeTaller.sacarVehiculoDeMantenimiento(idVehiculo);
                        System.out.println("\nEl vehiculo ha salido del mantenimiento satisfactoriamente\n");
                        break;
                    default:  
                        break;
                }
                break;
            
            case "4":
                
                System.out.println("\nSesión cerrada satisfactoriamente\n");
                break;
                
            default:
                System.out.println("\nHa ingresado una opción incorrecta\n");
                break;
        }
    }
    
    /**
     * Metodo para mostrar Vehiculos almacenados en entregables.txt
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void mostrarVehiculosPorEntregar()throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("entregables.txt");
        System.out.println("--VEHICULOS POR ENTREGAR--");
        System.out.println("---4------------------------------------------------------------------------------");//ERROR DE IMPRESION(REVISAR)
        for (String linea:lineas) {
            if(linea.split(",")[0].equals("mantenimiento")){
                System.out.print("CÓDIGO DEL VEHICULO:\t" + linea.split(",")[3]
                                 + "\nCÉDULA DEL CLIENTE:\t" + linea.split(",")[0]);
                System.out.println("\n---------------------------------------------------------------------------------");
            }
        }
    }

    /**
     * Metodo para mostrar todas las solicitudes de un tipo especifico
     * @param tipoSolicitud String que contiene el tipo de solicitud
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void mostrarSolicitudes(String tipoSolicitud) throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudes.txt");
        System.out.println("--SOLICITUDES DE " + tipoSolicitud.toUpperCase() + "--");
        System.out.println("---------------------------------------------------------------------------------");//ERROR DE IMPRESION(REVISAR)
        for (String linea:lineas) {
            if(linea.split(",")[0].equals(tipoSolicitud)){
                System.out.print("CÓDIGO DEL VEHICULO:\t" + linea.split(",")[3]
                                 + "\nCÉDULA DEL CLIENTE:\t" + linea.split(",")[0]);
                System.out.println("\n---------------------------------------------------------------------------------");
            }
        }
    }
    
    /**
     * Metodo para mostrar los Vehiculos que se encuentran en mantenimiento
     * @param tipoSolicitud String que contiene el tipo de solicitud
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void mostrarSolicitudesPendientes(String tipoSolicitud, String cedula)throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudes.txt");
        System.out.println("--SOLICITUDES DE " + tipoSolicitud.toUpperCase() + " PENDIENTES--");
        System.out.println("---------------------------------------------------------------------------------");//ERROR DE IMPRESION(REVISAR)
        for (String linea:lineas) {
            if((linea.split(",")[0].equals(tipoSolicitud)) && (linea.split(",")[3].equals(cedula))){
                System.out.print("CÓDIGO DEL VEHICULO:\t" + linea.split(",")[3]
                                 + "\nESTADO:\t PENDIENTE");
                System.out.println("\n---------------------------------------------------------------------------------");
            }
        }
    }
    
    /**
     * Metodo para mostrar las solicitudes aceptadas de un mismo tipo
     * @param tipoSolicitud String que contiene el tipo de solicitud
     * @param cedula String que contiene cedula de un Cliente
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void mostrarSolicitudesAceptadas(String tipoSolicitud, String cedula) throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudesfinalizadas.txt");
        System.out.println("--SOLICITUDES DE " + tipoSolicitud.toUpperCase() + " ACEPTADAS--");
        System.out.println("---------------------------------------------------------------------------------");//ERROR DE IMPRESION(REVISAR)
        for (String linea:lineas) {
            if((linea.split(",")[0].equals(tipoSolicitud)) && (linea.split(",")[3].equals(cedula)) && (linea.split(",")[2].equals("aceptado")) ){
                System.out.print("CÓDIGO DEL VEHICULO:\t" + linea.split(",")[1]
                                 + "\nESTADO:\t " + linea.split(",")[2] );
                System.out.println("\n---------------------------------------------------------------------------------");
            }
        }        
    }
    
    /**
     * Metodo para mostrar los Vehiculos que se encuentran en mantenimientos.txt
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void mostrarVehiculosEnMantenimiento() throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("mantenimientos.txt");
        System.out.println("--MANTENIMIENTOS--");
        System.out.println("---------------------------------------------------------------------------------");
        for (String linea:lineas) {
            System.out.print("CÓDIGO DEL VEHICULO:\t" + linea.split(",")[1]
                             + "\nCÉDULA DEL CLIENTE:\t" + linea.split(",")[0]
                             + "\nESTADO:\t" + linea.split(",")[3]);
            System.out.println("\n---------------------------------------------------------------------------------");
        }
    }
    
    /**
     * Metodo para mostrar los estados de mantenimiento de Vehiculos
     * @param cedulaCliente String que contiene la cedula de un Cliente
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void mostrarEstadoMantenimientoAutos(String cedulaCliente) throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("mantenimientos.txt");
        System.out.println("--MANTENIMIENTOS--");
        System.out.println("---------------------------------------------------------------------------------");
        for (String linea:lineas) {
            if(linea.split(",")[0].equals(cedulaCliente)){
                System.out.print("CÓDIGO DEL VEHICULO:\t" + linea.split(",")[1]
                               + "\nESTADO:\t" + linea.split(",")[3]);
                System.out.println("\n---------------------------------------------------------------------------------");
            }
        }
    }
    
    /**
     * Metodo que verifica si la solicitud de un tipo especifico tiene estado aceptado
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @param estadoSolicitud String que contiene el estado de la solicitud
     * @return valor booleano para verificar si la solicitud de un tipo especifico tiene estado aceptado
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public boolean tieneSolicitudAceptada(String idVehiculo, String estadoSolicitud) throws Exception {
        ArrayList<String> solicitudes = ManejoArchivos.leerArchivo("solicitudesFinalizadas.txt");
        for (String solicitud:solicitudes) {
            String datos[] = solicitud.split(";");
            if ((datos[1].equals(idVehiculo)) && datos[2].equals("aceptado")) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Metodo para verificar que la coincidencia usuario-contraseña existe
     * @param user String que contiene el nombre de usuario
     * @param pass String que contiene la contraseña del usuario
     * @return ArrayList de tipo String con un formato 
     * ["username","tipoUsuario","true"] si la coincidencia se da
     * ["","","false"] si la coincidencia no se da
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public ArrayList verificarLogin(String user, String pass) throws Exception {        
        ArrayList<String> info = new ArrayList();   // Contiene ["username","tipoUsuario","true"] o ["","","false"]
        ArrayList<String> lineas = new ArrayList(); // Almacenará contenido del archivo
        lineas = ManejoArchivos.leerArchivo("usuarios.txt");  // ["linea1\n",linea2\n...,lineaN\n]
        boolean match = false;  // Cambia de valor a true si encuentra un match user-pass en usuarios.txt
        for (String linea:lineas) {
            String[] datos = linea.split(",");
            if (datos[0].trim().equals(user.toLowerCase().trim()) && datos[1].trim().equals(pass.trim())) {
                match = true;
                info.add(datos[0]);
                info.add(datos[4]);
                info.add("true");
                System.out.println("\nIniciando sesión...");
                System.out.println("Acceso correcto\n");
            }
        }
        if (!match) {
            System.out.println("\nLas combinación de usuario y contraseña es incorrecta");
            info.add("");
            info.add("");
            info.add("false");
        }        
        return info;
    }
    
    /**
     * Metodo para hacer un downcast de Persona al tipo de usuario que hizo login
     * @param verificacion ArrayList de tipo String que contiene la verifiacion del login
     * @param fileName String que contiene el nombre del archivo con su respectiva extension
     * @return instancia de Persona del tipo de usuario que hizo login
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public Persona obtenerInfoUser(ArrayList<String> verificacion, String fileName) throws Exception {
        Persona user = new Persona();
        ArrayList<String> lineas = ManejoArchivos.leerArchivo(fileName);    // ALMACENA LINEAS DE tipo_usuario.txt
        ArrayList<String> datosPersona = new ArrayList();
        datosPersona = ManejoArchivos.leerArchivo("usuarios.txt");  // ALMACENA LINEAS DE usuarios.txt        
        for (String linea:lineas) {
            String[] userData = linea.split(",");   // DATOS DEL TIPO_USUARIO
            String[] match = new String[5];     // DATOS DEL USUARIO        
            for (String datos:datosPersona) {
                match = datos.split(",");
                if (userData[1].equals(verificacion.get(0)) && match[0].equals(verificacion.get(0))) {                
                    switch (verificacion.get(1)) {
                        case "cliente":
                            user = new Cliente(match[0], match[1], 
                                               match[2], match[3], 
                                               userData[0], userData[2],
                                               Float.parseFloat(userData[3]), Boolean.parseBoolean(userData[4]));
                            break;
                        case "vendedor":
                            user = new Vendedor(match[0], match[1], 
                                                match[2], match[3], 
                                                userData[0]);
                            break;
                        case "supervisor":
                            user = new Supervisor(match[0], match[1], 
                                                  match[2], match[3], 
                                                  new ArrayList<>(Arrays.asList(userData[2].split(";"))));
                            break;
                        case "jefe de taller":
                            user = new JefeTaller(match[0], match[1], 
                                                  match[2], match[3], 
                                                  new ArrayList<>(Arrays.asList(userData[2].split(";"))));
                        default:
                            break;
                    }
                }
            }
        }
        return user;
    }
    
    /**
     * Metodo para obtener los certificados de un Supervisor o Jefe de Taller
     * @param user String que contiene el nombre de usuario
     * @return ArrayList de tipo String que contiene
     * certificados academicos de un Supervisor
     * certificados tecnicos de un Jefe de Taller
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public ArrayList<String> obtenerCertificados(String user) throws Exception {
        ArrayList<String> certificaciones = new ArrayList();
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("certificaciones.txt");
        for (String linea:lineas) {
            if (linea.split(",")[1].equals(user)) {
                certificaciones.addAll(Arrays.asList(linea.split(";")));
            }
        }
        return certificaciones;
    }
}