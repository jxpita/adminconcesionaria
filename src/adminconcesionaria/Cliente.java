/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Una clase para representar a una Persona de tipo Cliente
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Cliente extends Persona implements Consultable {

    private String cedula;
    private String ocupacion;
    private double ingresos;
    private boolean tieneVehiculo;
    
    public Cliente(){
        this.tieneVehiculo = false;
    }

    public Cliente(String username, String password, String nombres, String apellidos, 
                   String cedula, String ocupacion, double ingresos, boolean tieneVehiculo) {
        super(username, password, nombres, apellidos);
        this.cedula = cedula;
        this.ocupacion = ocupacion;
        this.ingresos = ingresos;
        this.tieneVehiculo = tieneVehiculo;
    }
    
    /**
     * Metodo para verificar si el Cliente tiene fondos para pagar el mantenimiento
     * @param fondos contiene los ingresos de un Cliente
     * @param precio contiene el precio de reparacion de un Vehiculo
     * @return valor booleano si el Cliente tiene fondos para pagar el mantenimiento
     */
    public Boolean tieneFondos(Double fondos, Double precio){ // Metodo para comprobar si tiene fondos suficiente para hacer la transaccion
        return fondos>= precio;
    }
    
    @Override
    public void consultarStock() throws Exception {
        ArrayList<String> vehiculos = ManejoArchivos.leerArchivo("stock.txt");
        if (!vehiculos.isEmpty()) {
            System.out.println("---------------------------------------------------------------------------------");
            for (String vehiculo:vehiculos) {
                if (Boolean.parseBoolean(vehiculo.split(",")[12])) {
                    this.imprimirInfoVehiculo(vehiculo);
                    System.out.println("\n---------------------------------------------------------------------------------");
                }
            }
        } else {
            System.out.println("No hay vehiculos disponibles en stock!\n");
        }
    }
    
    /**
     * Metodo para imprimir las especificaciones basicas de un Vehiculo
     * @param vehiculo String que contiene todas las especificaciones de un Vehiculo
     */
    public void imprimirInfoVehiculo(String vehiculo) {
        System.out.print("CÓDIGO:\t\t\t" + vehiculo.split(",")[0]
                       + "\nTIPO DE VEHÍCULO:\t"
                       + vehiculo.split(",")[2].substring(0, 1).toUpperCase()
                       + vehiculo.split(",")[2].substring(1)
                       + "\nMARCA:\t\t\t"
                       + vehiculo.split(",")[3].substring(0, 1).toUpperCase() 
                       + vehiculo.split(",")[3].substring(1)
                       + "\nMODELO:\t\t\t" 
                       + vehiculo.split(",")[4].substring(0, 1).toUpperCase() 
                       + vehiculo.split(",")[4].substring(1)
                       + "\nAÑO DE FABRICACIÓN:\t" + vehiculo.split(",")[5]);                       
    }
    
    /**
     * Metodo para que el Cliente solicite la cotizacion de un Vehiculo
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void solicitarCotizacion(String idVehiculo) throws Exception {
        ArrayList<String> vehiculos = ManejoArchivos.leerArchivo("stock.txt");
        Random random = new Random();
        boolean disponible = true;
        for (String vehiculo:vehiculos) {
            if (Boolean.parseBoolean(vehiculo.split(",")[12])) {
                if (vehiculo.split(",")[0].equals(idVehiculo)) {
                    disponible = false;
                    this.cambiarDisponibilidadStock(idVehiculo);    // CAMBIA DISPONIBILIDAD EN stock.txt
                    ArrayList<String> vendedores = ManejoArchivos.leerArchivo("vendedores.txt");
                    String idVendedorAleatorio = vendedores.get(random.nextInt(vendedores.size())).split(",")[0];
                    String nuevaLinea = "cotizacion" + "," + this.cedula + "," + idVendedorAleatorio + "," + idVehiculo;
                    if (!ManejoArchivos.leerArchivo("solicitudes.txt").contains(nuevaLinea + "\n")) {
                        ManejoArchivos.escribirEnArchivo("solicitudes.txt", nuevaLinea);
                        System.out.println("\nUsted ha solicitado una cotización de vehículo con código:   " + idVehiculo);
                    }
                }
            } 
        }
        if (disponible) {
            System.out.println("\nEstimado(a) " + super.getNombres().split(" ")[0] + " " + 
                               super.getApellidos().split(" ")[0] + ", " + "ese vehículo no se encuentra disponible " +
                               "o no se encuentra registrado en el stock de la concesionaria\n");
        }
    }
    
    /**
     * Metodo para cambiar la disponibilidad de un Vehiculo
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void cambiarDisponibilidadStock(String idVehiculo) throws Exception{
        ArrayList<String> vehiculos = ManejoArchivos.leerArchivo("stock.txt");
        String nuevaLinea;
        int num = 0;
        for (String linea:vehiculos) {
            String[] datos = linea.split(",");            
            if (datos[0].equals(idVehiculo)){
                nuevaLinea = datos[0] + "," + datos[1] + "," + datos[2] + "," +
                             datos[3] + "," + datos[4] + "," + datos[5] + "," +
                             datos[6] + "," + datos[7] + "," + datos[8] + "," +
                             datos[9] + "," + datos[10] + "," + datos[11] + "," + "false";
                vehiculos.set(num, nuevaLinea);
                ManejoArchivos.sobreescribirArchivo("stock.txt", vehiculos);
            }
            num ++;
        }
    }
    
    /**
     * Metodo para imprimir las todas las especificaciones de un Vehiculo
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void imprimirInfoAdicionalVehiculo(String idVehiculo) throws Exception {
        ArrayList<String> vehiculos = ManejoArchivos.leerArchivo("stock.txt");
        for (String vehiculo:vehiculos) {
            if (vehiculo.contains(idVehiculo)) {
                this.imprimirInfoVehiculo(vehiculo);
                System.out.println("\nTIPO DE COMBUSTIBLE:\t" + vehiculo.split(",")[6] +
                                   "\nNÚMERO DE LLANTAS:\t" + vehiculo.split(",")[7]);
                if (vehiculo.contains("automovil")) {
                    System.out.println("NÚMERO DE ASIENTOS:\t" + vehiculo.split(",")[8]);
                    if (Boolean.parseBoolean(vehiculo.split(",")[9])) {
                        System.out.println("CONVERTIBLE:\t\tSí");
                    } else {
                        System.out.println("CONVERTIBLE:\t\tNo");
                    }
                    if (Boolean.parseBoolean(vehiculo.split(",")[10])) {
                        System.out.println("TIENE CAMARA RETRO:\tSí");
                    } else {
                        System.out.println("TIENE CAMARA RETRO:\tNo");
                    }
                } else if (vehiculo.contains("motocicleta")) {
                    System.out.println("CATEGORÍA:\t\t" + vehiculo.split(",")[8]);
                } else if (vehiculo.contains("tractor")) {
                    if (Boolean.parseBoolean(vehiculo.split(",")[8])) {
                        System.out.println("PARA USO AGRÍCOLA:\tSí");
                    } else {
                        System.out.println("PARA USO AGRÍCOLA:\tNo");
                    }
                    System.out.println("TIPO DE TRANSMISIÓN:\t" + vehiculo.split(",")[9]);
                } else {
                    System.out.println("CAPACIDAD DE CARGA:\t" + vehiculo.split(",")[8]
                                     + "\nNÚMERO DE EJES:\t\t" + vehiculo.split(",")[9]);
                }
                System.out.println("PRECIO:\t\t\t$ " + vehiculo.split(",")[11]);
                break;
            }            
        }
    }
    
    /**
     * Metodo para que un Cliente solicite la compra de un Vehiculo
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void solicitarCompra(String idVehiculo) throws Exception {
        if (this.tieneSolicitudAceptada(idVehiculo,"compra")) {
            Random random = new Random();
            ArrayList<String> supervisores = ManejoArchivos.leerArchivo("supervisores.txt");
            String idSupervisorAleatorio = supervisores.get(random.nextInt(supervisores.size())).split(",")[0];
            String nuevaLinea = "compra" + "," + this.cedula + "," + idSupervisorAleatorio + "," + idVehiculo;
            if (!ManejoArchivos.leerArchivo("solicitudes.txt").contains(nuevaLinea + "\n")) {
                ManejoArchivos.escribirEnArchivo("solicitudes.txt", nuevaLinea);
                System.out.println("\nUsted ha solicitado la compra de vehículo con código:   " + idVehiculo);
            }
        } else {
            System.out.println("\nNo puede solicitar la compra de ese vehículo porque su cotización no está aprobada\n");
        }
    }

    /**
     * Metodo para que un Cliente solicite el mantenimiento de un Vehiculo
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void solicitarMantenimiento(String idVehiculo) throws Exception {
        if(this.tieneVehiculo){
            Scanner sc = new Scanner(System.in);
            System.out.println("Ingrese el codigo del Vehiculo que quiere meter a mantenimiento");
            System.out.println("\nQue tipo de mantenimiento desea usar: ");
            System.out.println("1. Mantenimiento preventivo.\n"
                             + "2. Mantenimiento de emergencia. \n");
            System.out.println("Elija una opcion: ");
            String opcionMantenimiento = sc.next();
            switch (opcionMantenimiento) {
                case "1":
                    ManejoArchivos.escribirEnArchivo("solicitudes.txt", "mantenimiento" + "," + this.cedula + "," + "," + "preventivo" + "," + idVehiculo);
                    break;
                case "2":
                    ManejoArchivos.escribirEnArchivo("solicitudes.txt", "mantenimiento" + "," + this.cedula + "," + "," + "emergencia" + "," + idVehiculo);
                    break;
                default:
                    break;
            }
        }
        else{
            System.out.println("\nUsted no puede solicitar mantenimiento porque no tiene ni un vehiculo comprado por la concesionaria\n");
        }
    }
    
    /**
     * Metodo que verifica si la solicitud de un Cliente ha sido aceptada
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @param tipoSolicitud String que contiene el tipo de solicitud
     * @return valor booleano que determina si la solicitud esta aceptada
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
     public boolean tieneSolicitudAceptada(String idVehiculo, String tipoSolicitud) throws Exception {
        ArrayList<String> solicitudes = ManejoArchivos.leerArchivo("solicitudesFinalizadas.txt");
        for (String solicitud:solicitudes) {
            String datos[] = solicitud.split(";");
            if ((datos[1].equals(idVehiculo)) && datos[0].equals(tipoSolicitud) && datos[1].equals("aceptada")) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Metodo para que un Cliente pague el mantenimiento de un Vehiculo
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @param costo contiene valor decimal del costo de mantenimiento
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void pagarDeudaMantenimiento(String idVehiculo, Double costo) throws Exception{
        if(this.tieneFondos(this.ingresos, costo)){
            ArrayList<String> lineas = new ArrayList();
            lineas = ManejoArchivos.leerArchivo("entregables.txt");
            String nuevaLinea = "";
            int num = 0;
            for (String linea:lineas) {                                       
                String[] datos = linea.split(",");            
                if (datos[1].equals(idVehiculo) ){
                    nuevaLinea = datos[0] + "," + datos[1] + "," + datos[2] + "," + true;
                    lineas.set(num, nuevaLinea);
                    System.out.println(lineas);
                    ManejoArchivos.sobreescribirArchivo("entregables.txt", lineas);
                }
            num ++;
            }
        }
        else {
            System.out.println("No tiene dinero suficiente para realizar el pago");
        }
    }

    /**
     * Metodo que verifica si el Cliente tiene un Vehiculo
     * @return valor booleano que determina si el Cliente tiene un Vehiculo
     */
    public boolean isTieneVehiculo() {
        return tieneVehiculo;
    }

    /**
     * Metodo que modifica valor booleano si el Cliente tiene un Vehiculo
     * @param tieneVehiculo valor booleano que determina si el Cliente tiene un Vehiculo
     */
    public void setTieneVehiculo(boolean tieneVehiculo) {
        this.tieneVehiculo = tieneVehiculo;
    }

    /**
     * Metodo para obtener la cedula de un Cliente
     * @return String que contiene la cedula de un Clienta
     */
    public String getCedula() {
        return cedula;
    }
    
    /**
     * Metodo para obtener los ingresos de un Cliente
     * @return decimal que contiene los ingresos de un Cliente
     */
    public double getIngresos() {
        return ingresos;
    }
    
    @Override
    public String toString() {
        return super.toString() + "Cliente{" + "cedula=" + cedula + ", ocupacion=" + ocupacion + ", ingresos=" + ingresos + 
               ", tieneVehiculo=" + tieneVehiculo + '}';
    
    }
}