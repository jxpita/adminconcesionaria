/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

import java.util.Scanner;

/**
 *
 * @author Juan Xavier Pita
 */
public class AdminConcesionaria {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        
        Menu menu = new Menu();
        Scanner scan = new Scanner(System.in);
        
        String opcion = "";
        
        while (!opcion.equals("3")) {
            menu.menuPrincipal();
            opcion = scan.next().trim();
            menu.loginMenuPrincipal(opcion);            
        }
        
    }
    
}
