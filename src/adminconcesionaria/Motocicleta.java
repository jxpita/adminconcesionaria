/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

/**
 * Una clase que representa un Vehiculo de tipo Motocicleta
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Motocicleta extends Vehiculo{
    private String categoria; // De tipo Deportiva, Scooter y todo Terreno

    public Motocicleta() {
        super();
    }

    public Motocicleta(String categoria, String marca, String modelo, String anioDeFabricacion, String tipoGasolina, int kilometraje) {
        super(marca, modelo, anioDeFabricacion, tipoGasolina, 2, kilometraje);// Solo tiene 2 llantas
        this.categoria = categoria;
    }

    /**
     * Metodo para obtener la categoria de la Motocicleta
     * @return valor del atributo categoria que describe un subtipo de Motocicleta
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * Metodo para modificar la categoria de la Motocicleta
     * @param categoria representa el subtipo de la Motocicleta
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}

