/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Una clase para leer, escribir y modificar archivos
 * Sus metodos estaticos permiten llamar a los metodos para el manejo de archivos
 * sin necesidad de crear instancias de la clase
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class ManejoArchivos {

    /**
     * Metodo estatico que lee un archivo
     * @param fileName String que contiene el nombre del archivo con su respectiva extension
     * @return ArrayList de tipo String con el contenido de las lineas del archivo
     * @throws Exception si el archivo no existe o no se pudo leer adecuadamente
     */
    public static ArrayList<String> leerArchivo (String fileName) throws Exception {
        File file = new File(fileName);
        FileReader fReader = null;
        BufferedReader bReader = null;
        ArrayList<String> lineas = new ArrayList();
        fReader = new FileReader(file);
        bReader = new BufferedReader(fReader);
        String linea = bReader.readLine();

        while(linea != null){
            lineas.add(linea);
            linea = bReader.readLine();
        }
        
        bReader.close();
        fReader.close();     
        return lineas;        
    } 
    
    /**
     * Metodo estatico que escribe en un archivo
     * @param fileName String que contiene el nombre del archivo con su respectiva extension
     * @param formato String de la nueva linea a escribir en el archivo
     * @throws Exception si el archivo no existe o no se pudo leer adecuadamente
     */
    public static void escribirEnArchivo(String fileName, String formato) throws Exception {        
        File file= new File(fileName);
        FileOutputStream fos = new FileOutputStream(file, true);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        bw.write(formato);
        bw.newLine();

        bw.close();
    }
    
    /**
     * Metodo estatico para añadir contenido nuevo a un archivo
     * @param direccion String que contiene el nombre del archivo con su respectiva extension
     * @param nuevosDatos String de la nueva linea a escribir al final del archivo
     * @throws Exception si el archivo no existe o no se pudo leer adecuadamente
     */
    public static void sobreescribirArchivo(String direccion, ArrayList<String> nuevosDatos) throws Exception {
        File file = new File(direccion);
        FileWriter fWriter= new FileWriter(file);
        PrintWriter pWriter= new PrintWriter(fWriter);
        Iterator<String> it= nuevosDatos.iterator();
        while (it.hasNext()){
            pWriter.println(it.next());
        }

        fWriter.close();
        pWriter.close();
    }    
}