/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Una clase para representar a una Persona de tipo Vendedor
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Vendedor extends Persona implements Consultable, Solicitable {
    private String idVendedor;

    public Vendedor() {
    }

    public Vendedor(String username, String password, String nombres, String apellidos, String idVendedor) {
        super(username, password, nombres, apellidos);
        this.idVendedor = idVendedor;
    }

    @Override
    public void consultarStock() throws Exception {
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("stock.txt");
        System.out.println("---------------------------------------------------------------------------------");
        for (String linea:lineas) {
            System.out.print("CÓDIGO:\t" + linea.split(",")[0]
                             + "\nMARCA:\t" + linea.split(",")[3]
                             + "\nMODELO:\t" + linea.split(",")[4]);
            System.out.println("\n---------------------------------------------------------------------------------");
        }
    }

    @Override
    public void aceptarSolicitud(String idVehiculo)throws Exception {
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudes.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");
            if (datos[3].equals(idVehiculo) ) {
                ManejoArchivos.escribirEnArchivo("solicitudesFinalizadas.txt", datos[0] + "," + idVehiculo + "," + "aceptado" + "," + datos[1] + "," + "Su solicitud de cotizacion ha sido aceptada.");
                lineas.remove(linea);
                ManejoArchivos.sobreescribirArchivo("solicitudes.txt", lineas); 
                break;
            }      
            if(lineas.isEmpty()){
                break;
            }
       }
    }

    @Override
    public void rechazarSolicitud(String idVehiculo) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("\nIngrese el motivo por el cual va a rechazar la solicitud: ");
        String mensaje = sc.next();
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudes.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");
                if (datos[3].equals(idVehiculo) ){
                    ManejoArchivos.escribirEnArchivo("solicitudesFinalizadas.txt",datos[0] + "," + idVehiculo + "," + "rechazado" + "," + datos[1] + "," + mensaje);
                    lineas.remove(linea);
                    ManejoArchivos.sobreescribirArchivo("solicitudes.txt", lineas);
                    break;
                }
            if(lineas.isEmpty()){
                break;
            }
        }
    }
    
    /**
     *
     * @return
     */
    public String getIdVendedor() {
        return idVendedor;
    }

    /**
     *
     * @param idVendedor
     */
    public void setIdVendedor(String idVendedor) {
        this.idVendedor = idVendedor;
    }

    @Override
    public String toString() {
        return super.toString() + "Vendedor{" + "idVendedor=" + idVendedor + '}';
    }
}

