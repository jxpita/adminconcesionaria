/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

/**
 * Interfaz Solicitable se implementa en Vendedor, Sueprvisor y JefeTaller para que puedan 
 * aceptar las solicitudes que hace un cliente
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public interface Solicitable {
    
    /**
     * Metodo para cambiar el estado de una solicitud a aceptado
     * @param idVehiculo String que contiene el identificador unico de Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    void aceptarSolicitud(String idVehiculo) throws Exception;

    /**
     * Metodo para cambiar el estado de una solicitud a rechazado
     * @param idVehiculo String que contiene el identificador unico de Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    void rechazarSolicitud(String idVehiculo) throws Exception;
    
}
