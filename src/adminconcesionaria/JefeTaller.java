/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Una clase para representar a una Persona de tipo JefeTaller
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class JefeTaller extends Persona implements Solicitable {
    private ArrayList<String> listaCertificacionesTecnicas;

    public JefeTaller() {
        super();
    }

    public JefeTaller(String username, String password, String nombres, String apellidos, ArrayList<String> listaCertificacionesTecnicas) {
        super(username, password, nombres, apellidos);
        this.listaCertificacionesTecnicas = listaCertificacionesTecnicas;
    }

    /**
     * Metodo para ingresar un Vehiculo a mantenimiento
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @param cedulaCliente String que contiene la cedula de un Cliente
     * @param tipoMantenimiento String que contiene el tipo de mantenimiento a realizar
     * @param kilometraje valor entero que contiene el kilometraje de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void ingresarVehiculoAMantenimiento(String idVehiculo, String cedulaCliente, String tipoMantenimiento , int kilometraje) throws Exception{ //Puede ir en el mismo main. RECORDAR QUE LA DISPONIBILIDAD DEL CARRO DEL CLIENTE DEBE DE CAMBIAR
        if(tipoMantenimiento.equals("preventivo")){
            ManejoArchivos.escribirEnArchivo("mantenimientos.txt", cedulaCliente + "," + idVehiculo + "," + (0.10*kilometraje) + "admitido");
        }
        else if(tipoMantenimiento.equals("emergencia")){
            Scanner sc = new Scanner(System.in);
            System.out.println("Ingrese el costo por el mantenimiento por emergencia:");
            String costoEmergencia = sc.next();
            ManejoArchivos.escribirEnArchivo("mantenimientos.txt", cedulaCliente + "," + idVehiculo + "," + costoEmergencia + "admitido");
        }
        this.cambiarEstadoMantenimientoVehiculoCliente(idVehiculo, true);
    }
    
    /**
     * Metodo para obtener el kilometraje actual de un Vehiculo
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @return valor entero que contiene el kilometraje actual de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public int obtenerKilometrajeActual(String idVehiculo) throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("vehiculosVendidos.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");            
            if (datos[1].equals(idVehiculo) ){
                return Integer.parseInt(datos[2]);
            }
        }
        return 0;
    }
    
    /**
     * Metodo para obtener la solicitud de un tipo especifico
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @return Array que contiene una solicitud de un tipo especifico
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public String[] obtenerSolicitud(String idVehiculo) throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudes.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");            
            if (datos[1].equals(idVehiculo) ){
                return datos;
            }
        }
        return null;
    }
    
    /**
     * Metodo para entregar un Vehiculo comprado o que sale de mantenimiento
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void entregarVehiculo(String idVehiculo) throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("entregables.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");
            if(datos[1].equals(idVehiculo)){
                if(datos[3].equals("true")){
                    this.cambiarEstadoMantenimientoVehiculoCliente(idVehiculo, false);
                    lineas.remove(linea);
                    ManejoArchivos.sobreescribirArchivo("entregables.txt", lineas);
                    System.out.println("El vehiculo ha sido entregado satisfactoriamente\n");
                    this.eliminarVehiculoStock(idVehiculo);
                    break;
                }
                else if(datos[3].equals("false")){
                    System.out.println("El cliente no ha pagado el mantenimiento");
                    break;
                }
            }
            if(lineas.isEmpty()){
                break;
            }
        }
    
    }
    
    /**
     * Metodo para eliminar un Vehiculo del stock de la concesionaria luego de haber sido comprado
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void eliminarVehiculoStock(String idVehiculo)throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("stock.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");
            if (datos[1].equals(idVehiculo) ) {
                lineas.remove(linea);
                ManejoArchivos.sobreescribirArchivo("mantenimientos.txt", lineas);
                break;
            }      
            if(lineas.isEmpty()){
                break;
            }
        }
    }
    
    /**
     * Metodo para cambiar un estado de mantenimiento especifico de Vehiculo
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @param enMantenimiento valor booleano que determina si un Vehiculo esta en mantenimiento
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void cambiarEstadoMantenimientoVehiculoCliente(String idVehiculo, Boolean enMantenimiento)throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("vehiculosVendidos.txt");
        String nuevaLinea = "";
        int num = 0;
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");            
            if (datos[1].equals(idVehiculo) ){
                nuevaLinea = datos[0] + "," + datos[1] + "," + datos[2] + "," 
                            + datos[3] + "," + datos[4] + "," + datos[5] + ","
                            + datos[6] + "," + datos[7] + "," + datos[8] + ","
                            + datos[9] + "," + datos[10] + "," + datos[11] + ","
                            + datos[12] + "," + datos[13] + "," + enMantenimiento;
                lineas.set(num, nuevaLinea);
                System.out.println(lineas);
                ManejoArchivos.sobreescribirArchivo("vehiculosVendidos.txt", lineas);
            }
            num ++;
        }
    }
    
    /**
     * Metodo que cambia el estado de mantenimiento de un Vehiculo
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @param estado String que contiene el estado de mantenimiento
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void cambiarEstadoVehiculo (String idVehiculo, String estado) throws Exception{
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("mantenimientos.txt");
        String nuevaLinea = "";
        int num = 0;
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");            
            if (datos[1].equals(idVehiculo) ){
                nuevaLinea = datos[0] + "," + datos[1] + "," + datos[2] + "," + estado;
                lineas.set(num, nuevaLinea);
                System.out.println(lineas);
                ManejoArchivos.sobreescribirArchivo("mantenimientos.txt", lineas);
            }
            num ++;
        }
    }
    
    /**
     * Metodo para verificar si un Vehiculo esta listo para salir de mantenimiento
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @return valor booleano que verifica si el Vehiculo puede salir de un mantenimiento
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public boolean verificarSalidaVehiculo(String idVehiculo) throws Exception {
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("mantenimientos.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");            
            if (datos[1].equals(idVehiculo) ){
                if(datos[3].equals("en etapa a prueba")){
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Metodo para sacar un Vehiculo de mantenimiento
     * @param idVehiculo String que contiene el identificador unico de un Vehiculo
     * @throws Exception si algun archivo no existe o no se pudo leer adecuadamente
     */
    public void sacarVehiculoDeMantenimiento(String idVehiculo) throws Exception{
        if(this.verificarSalidaVehiculo(idVehiculo) == true) {
            ArrayList<String> lineas = new ArrayList();
            lineas = ManejoArchivos.leerArchivo("mantenimientos.txt");
            for (String linea:lineas) {                                       
                String[] datos = linea.split(",");
                if (datos[1].equals(idVehiculo) ) {
                    ManejoArchivos.escribirEnArchivo("entregables.txt", datos[0] + "," + datos[1] + "," + datos[2]);
                    lineas.remove(linea);
                    ManejoArchivos.sobreescribirArchivo("mantenimientos.txt", lineas);
                    break;
                }      
                if(lineas.isEmpty()){
                    break;
                }
            }
        }
        else{
            System.out.println("\nEl vehiculo aun no ha entrado a reparacion o aun no esta reparado para ser entregado\n");
        }
    }
    
    @Override
    public void aceptarSolicitud(String idVehiculo)throws Exception {
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudes.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");
            if (datos[3].equals(idVehiculo) ) {
                ManejoArchivos.escribirEnArchivo("solicitudesFinalizadas.txt", datos[0] + "," + idVehiculo + "," + "aceptado" + "," + datos[1] + "," + "Su solicitud de mantenimiento ha sido aceptada.");// PONER FORMATO POR ENTREGAR
                lineas.remove(linea);
                ManejoArchivos.sobreescribirArchivo("solicitudes.txt", lineas); 
                break;
            }      
            if(lineas.isEmpty()){
                break;
            }
       }
    }

    @Override
    public void rechazarSolicitud(String idVehiculo) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("\nIngrese el motivo por el cual va a rechazar la solicitud: ");
        String mensaje = sc.next();
        ArrayList<String> lineas = new ArrayList();
        lineas = ManejoArchivos.leerArchivo("solicitudes.txt");
        for (String linea:lineas) {                                       
            String[] datos = linea.split(",");
                if (datos[3].equals(idVehiculo) ){
                    ManejoArchivos.escribirEnArchivo("solicitudesFinalizadas.txt",datos[0] + "," + idVehiculo + "," + "rechazado" + "," + datos[1] + "," + mensaje);
                    lineas.remove(linea);
                    ManejoArchivos.sobreescribirArchivo("solicitudes.txt", lineas);
                    break;
                }
            if(lineas.isEmpty()){
                break;
            }
        }
    }
    
    /**
     * Metodo para obtener las certificaciones tecnicas del Jefe de Taller
     * @return ArrayList de tipo String que contiene las certificaciones tecnicas del Jefe de Taller
     */
    public ArrayList<String> getListaCertificacionesTecnicas() {
        return listaCertificacionesTecnicas;
    }

    /**
     * Metodo para modificar las certificaciones tecnicas del Jefe de Taller
     * @param listaCertificacionesTecnicas ArrayList de tipo String que contiene las certificaciones 
     * tecnicas del Jefe de Taller
     */
    public void setListaCertificacionesTecnicas(ArrayList<String> listaCertificacionesTecnicas) {
        this.listaCertificacionesTecnicas = listaCertificacionesTecnicas;
    }

    @Override
    public String toString() {
        return super.toString() + "JefeTaller{" + "listaCertificacionesTecnicas=" + listaCertificacionesTecnicas + '}';
    }
    
}

