/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

/**
 * Una clase que representa un Vehiculo
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Vehiculo {
    private String marca;
    private String modelo;
    private String anioDeFabricacion;
    private String tipoGasolina;
    private int numLlantas;
    private int kilometraje;
    private boolean estaDisponible = true;  // Si es false no se puede mostrar a otro cliente porque
                                            // ya se encuentra solicitado
    
    public Vehiculo() {
    }

    public Vehiculo(String marca, String modelo, String anioDeFabricacion, String tipoGasolina, int numLlantas, int kilometraje) {
        this.marca = marca;
        this.modelo = modelo;
        this.anioDeFabricacion = anioDeFabricacion;
        this.tipoGasolina = tipoGasolina;
        this.numLlantas = numLlantas;
        this.kilometraje = kilometraje;
    }

    /**
     * Metodo para obtener la marca de un Vehiculo
     * @return String que representa la marca de un Vehiculo
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Metodo para modificar la marca de un Vehiculo
     * @param marca representa String de la marca de un Vehiculo
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * Metodo para obtener el modelo de un Vehiculo
     * @return String que representa el modelo de un Vehiculo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Metodo para modificar el modelo de un Vehiculo
     * @param modelo representa String del modelo de un Vehiculo
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * Metodo para obtener el año de fabricacion de un Vehiculo
     * @return String anioFabricacion que representa el año de fabricacion de un Vehiculo
     */
    public String getAnioDeFabricacion() {
        return anioDeFabricacion;
    }

    /**
     * Metodo para modificar el año de fabricacion de un Vehiculo
     * @param anioDeFabricacion String que representa el año de fabricacion de un Vehiculo
     */
    public void setAnioDeFabricacion(String anioDeFabricacion) {
        this.anioDeFabricacion = anioDeFabricacion;
    }

    /**
     * Metodo para obtener el tipo de combustible que usa un Vehiculo
     * @return String que representa el tipo de combustible que usa un Vehiculo
     */
    public String getTipoGasolina() {
        return tipoGasolina;
    }

    /**
     * Metodo para modificar el tipo de combustible que usa un Vehiculo
     * @param tipoGasolina String que representa el tipo de combustible que usa un Vehiculo
     */
    public void setTipoGasolina(String tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    /**
     * Metodo para obtener el número de llantas que tiene un Vehiculo
     * @return valor entero que representa el numero de llantas que tiene un Vehiculo
     */
    public int getNumLlantas() {
        return numLlantas;
    }

    /**
     * Metodo para modificar el numero de llantas que tiene un Vehiculo
     * @param numLlantas valor entero que representa el numero de llantas que tiene un Vehiculo
     */
    public void setNumLlantas(int numLlantas) {
        this.numLlantas = numLlantas;
    }

    /**
     * Metodo para obtener el kilometraje de un Vehiculo
     * @return valor entero que representa el kilometraje de un Vehiculo
     */
    public int getKilometraje() {
        return kilometraje;
    }

    /**
     * Metodo para modificar el kilometraje de un Vehiculo
     * @param kilometraje valor entereo que representa el kilometraje de un Vehiculo
     */
    public void setKilometraje(int kilometraje) {
        this.kilometraje = kilometraje;
    }

    /**
     * Metodo que verifica si el Vehiculo se encuentra disponible para cotizar
     * @return valor booleano que describe la disponibilidad de cotizacion de un Vehiculo
     */
    public boolean isEstaDisponible() {
        return estaDisponible;
    }

    /**
     * Metodo modifica la disponibilidad de cotizacion del Vehiculo
     * @param estaDisponible valor booleano que representa la disponibilidad de contizacion de un Vehiculo
     */
    public void setEstaDisponible(boolean estaDisponible) {
        this.estaDisponible = estaDisponible;
    }
}