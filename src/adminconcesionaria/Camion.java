/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

/**
 * Una clase para representar un Vehiculo de tipo Camion
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Camion extends Vehiculo {
    private float capacidadDeCarga;
    private int numDeEjes;  // Numero de llantas /2 

    public Camion() {
        super();
    }

    public Camion(float capacidadDeCarga, int numDeEjes, String marca, String modelo, String anioDeFabricacion, String tipoGasolina, int numLlantas, int kilometraje) {
        super(marca, modelo, anioDeFabricacion, tipoGasolina, numLlantas, kilometraje);// Puede tener 4, 6 u 8 llantas.
        this.capacidadDeCarga = capacidadDeCarga;
        this.numDeEjes = numDeEjes;
    }

    /**
     * Metodo para obtener el valor del atributo capacidadDeCarga
     * @return atributo capacidadDeCarga que describe un numero decimal de la carga que soporta el camion
     */
    public float getCapacidadDeCarga() {
        return capacidadDeCarga;
    }

    /**
     * Metodo para modificar el atributo capacidadDeCarga
     * @param capacidadDeCarga describe un numero decimal de la carga que soporta el camion
     */
    public void setCapacidadDeCarga(int capacidadDeCarga) {
        this.capacidadDeCarga = capacidadDeCarga;
    }

    /**
     * Metodo para obtener el valor del atributo numDeEjes
     * numDeEjes = super.getNumLlantas()/2
     * @return atributo numDeEjes que describe un entero del numero de ejes que tiene el camion
     */
    public int getNumDeEjes() {
        return numDeEjes;
    }

    /**
     * Metodo para modificar el atributo numDeEjes
     * numDeEjes = super.getNumLlantas()/2
     * @param numDeEjes representa el numero de ejes del camion
     */
    public void setNumDeEjes(int numDeEjes) {
        this.numDeEjes = numDeEjes;
    }
}
