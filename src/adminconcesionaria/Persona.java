/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminconcesionaria;

/**
 * Una clase para representar a una Persona
 * @author Juan Xavier Pita
 * @author Daniel Vargas
 */
public class Persona {

    private String username;
    private String password;
    private String nombres;
    private String apellidos;
    
    public Persona(){
    }
    
    public Persona(String username, String password, String nombres, String apellidos) {
        this.username = username;
        this.password = password;
        this.nombres = nombres;
        this.apellidos = apellidos;
    }
    
    /**
     * Metodo para obtener el nombre de usuario de la Persona
     * @return String que contiene el nombre de usuario de la Persona
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * Metodo para modificar el nombre de usuario de la Persona
     * @param username String que contiene el nombre de usuario de la Persona
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Metodo para obtener la contraseña de la Persona
     * @return String que contiene la contraseña de la Persona
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * Metodo para modificar la contraseña de la Persona
     * @param password String que contiene la contraseña de la Persona
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Metodo para obtener los dos nombres de la Persona
     * @return String que contiene los dos nombres de la Persona
     */
    public String getNombres() {
        return nombres;
    }
    
    /**
     * Metodo para modificar los dos nombres de la Persona
     * @param nombres String que contiene los dos nombres de la Persona
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * Metodo para obtener los dos apellidos de la Persona
     * @return String que contiene los dos apellidos de la Persona
     */
    public String getApellidos() {
        return apellidos;
    }
    
    /**
     * Metodo para modificar los dos apellidos de la Persona
     * @param apellidos String que contiene los dos apellidos de la Persona
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Override
    public String toString() {
        return "Persona{" + "username=" + username + ", password=" + password + ", nombres=" + nombres + ", apellidos=" + apellidos + '}';
    }
}
